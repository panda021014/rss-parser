module lab8

go 1.16

require (
	github.com/jackc/pgx/v4 v4.11.0
	github.com/mmcdole/gofeed v1.1.3
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/text v0.3.7 // indirect
)
